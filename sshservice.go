/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://server.io/contact/ when you want license
this software under a commercial license.
*/

package ssh

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/perinet/generic/lib/httpserver"
	"gitlab.com/perinet/generic/lib/httpserver/periHttp"
	"gitlab.com/perinet/generic/lib/httpserver/rbac"
	"gitlab.com/perinet/generic/lib/utils/filestorage"
	"gitlab.com/perinet/generic/lib/utils/shellhelper"
)

func PathsGet() []httpserver.PathInfo {
	return []httpserver.PathInfo{
		{Url: "/ssh", Method: httpserver.GET, Role: rbac.USER, Call: SSH_Info_Get},
		{Url: "/ssh/config", Method: httpserver.GET, Role: rbac.ADMIN, Call: SSH_Config_Get},
		{Url: "/ssh/config", Method: httpserver.PATCH, Role: rbac.ADMIN, Call: SSH_Config_Set},
		{Url: "/ssh/otp", Method: httpserver.GET, Role: rbac.ADMIN, Call: SSH_OTP_Get},
	}
}

const (
	API_VERSION = "23"
)

type SSHInfo struct {
	ApiVersion json.Number `json:"api_version"`
}

type OTPInfo struct {
	OTP uint64 `json:"otp"`
}

type SSHConfig struct {
	Enabled bool `json:"enabled"`
}

var (
	config  		= SSHConfig{Enabled: false}
	CONFIG_FILE   	= "/var/lib/api_service/ssh-config.json"
	prefixPATH 		= ""
)

func init(){
	err := filestorage.LoadObject(CONFIG_FILE, &config)
	if err != nil {
		log.Println("error reading ssh configuration file", err)
	}
}

func SSH_Info_Get(p periHttp.PeriHttp) {
	binaryJson, err := json.Marshal(SSHInfo{ApiVersion: API_VERSION})
	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	p.JsonResponse(http.StatusOK, binaryJson)
}

func SSH_OTP_Get(p periHttp.PeriHttp) {
	
	if !config.Enabled {
		p.JsonResponse( http.StatusServiceUnavailable, json.RawMessage(`{"error": "SSH service is disabled!"}`))
		return
	} else {
	
		res := shellhelper.ShellCall("oathtool --totp -d6 $(grep -m 1 root /etc/users.oath | awk -F ' ' '{print $4}')")

		otp, err := strconv.ParseUint(strings.TrimSuffix(res, "\n"), 10, 64)
		if err != nil {
			p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
			return
		}

		p.JsonResponse(http.StatusOK, OTPInfo{OTP: otp})
	}
}

func SSH_Config_Get(p periHttp.PeriHttp) {
	var http_status int = http.StatusOK

	//prepare reponse
	res, err := json.Marshal(config)
	if err != nil {
		log.Println(" SSH_Config_Get ", err.Error())
		http_status = http.StatusInternalServerError
		res = json.RawMessage(`{"error": "` + err.Error() + `"}`)
	}

	// send json response
	p.JsonResponse( http_status, res)

}

func SSH_Config_Set(p periHttp.PeriHttp) {
	// read tha payload received in request
	payload, _ := p.ReadBody()

	var sshConfig_tmp SSHConfig = config

	// unmarshall the payload into the temp config and return error if it doesn't work
	err := json.Unmarshal(payload, &sshConfig_tmp)
	if err != nil {
		p.JsonResponse( http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	// when everything goes well set the config with the new received
	config = sshConfig_tmp

	// store the new config
	err = filestorage.StoreObject(prefixPATH + CONFIG_FILE, &config)
	if err != nil {
		p.JsonResponse( http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	// start/stop sshd service
	if config.Enabled {
		shellhelper.ShellCall("service sshd start")
	} else {
		shellhelper.ShellCall("service sshd stop")
	}

	// everything ok, update the status to NO CONTENT
	p.EmptyResponse(http.StatusNoContent)
}