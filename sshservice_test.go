/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package ssh

import (
	"encoding/json"
	"io"
	"net/http"
	"testing"
	"os"

	"gitlab.com/perinet/generic/lib/utils/intHttp"
	"gitlab.com/perinet/generic/lib/utils/shellhelper"
	"gotest.tools/v3/assert"
)

func TestMain(t *testing.T) {
	dir := "_cache/var/lib/api_service/"
	os.RemoveAll(dir)
	prefixPATH = "_cache"
}

func TestSshInfo(t *testing.T) {
	var info SSHInfo
	data := intHttp.Get(SSH_Info_Get, nil)
	err := json.Unmarshal(data, &info)

	assert.NilError(t, err)
	assert.Assert(t, info.ApiVersion == "23")
}

func TestSSHConfig(t *testing.T) {
	resp := intHttp.Call(SSH_OTP_Get, "GET", nil, nil)
	assert.Assert(t, resp.StatusCode == http.StatusServiceUnavailable)
	
	var config SSHConfig
	
	data := intHttp.Get(SSH_Config_Get, nil)
	err := json.Unmarshal(data, &config)
	assert.NilError(t, err)
	assert.Assert(t, config.Enabled == false)

	config.Enabled = true
	data, err = json.Marshal(config)
	assert.Assert(t, err == nil)
	intHttp.Patch(SSH_Config_Set, data, nil)

	data = intHttp.Get(SSH_Config_Get, nil)
	var sshConfig_test SSHConfig
	err = json.Unmarshal(data, &sshConfig_test)
	assert.Assert(t, err == nil)
	assert.Assert(t, sshConfig_test == config)
}

func TestNetworkInterfaces(t *testing.T) {
	shellhelper.ShellCall = func(cmd_ string) string {
		if cmd_ == "oathtool --totp -d6 $(grep -m 1 root /etc/users.oath | awk -F ' ' '{print $4}')" {
			return "123456"
		} else {
			t.Log("error: unexpected shell call, return empty string:", cmd_)
			return ""
		}
	}
	defer func() { shellhelper.ShellCall = shellhelper.Default_ShellCall }()

	resp := intHttp.Call(SSH_OTP_Get, "GET", nil, nil)

	assert.Assert(t, resp.StatusCode == http.StatusOK)

	var otpInfo OTPInfo
	data, _ := io.ReadAll(resp.Body)
	err := json.Unmarshal(data, &otpInfo)

	assert.NilError(t, err)
	assert.Assert(t, otpInfo == OTPInfo{OTP: 123456})
}