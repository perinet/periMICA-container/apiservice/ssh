# API Service SSH

The apiservice SSH is the implementation of the ssh part from the
[unified api](https://gitlab.com/perinet/unified-api).

## References

* [openAPI spec for apiservice ssh] https://gitlab.com/perinet/unified-api/-/blob/main/services/SSH_openapi.yaml


# Dual License

This software is by default licensed via the GNU Affero General Public License
version 3. However it is also available with a commercial license on request
(https://perinet.io/contact).